import React, { useEffect, useState } from "react";
import { type RecipeData } from "../datatypes";
import { FaStar, FaStarHalfAlt } from "react-icons/fa";

const colors = [
  "bg-red-300",
  "bg-yellow-300",
  "bg-green-300",
  "bg-blue-300",
  "bg-indigo-300",
  "bg-purple-300",
  "bg-pink-300",
  "bg-gray-300",
  "bg-orange-300",
  "bg-teal-300",
];

function Rating(props: { rating: number }): JSX.Element {
  const fullStars = Math.floor(props.rating);
  const hasHalfStar = props.rating % 1 >= 0.5;

  return (
    <div className="flex items-center">
      {[...Array(fullStars)].map((_, index) => (
        <FaStar key={index} className="text-yellow-500 w-4 h-4" />
      ))}
      {hasHalfStar && <FaStarHalfAlt className="text-yellow-500 w-4 h-4" />}
      {[...Array(5 - fullStars - (hasHalfStar ? 1 : 0))].map((_, index) => (
        <FaStar key={index} className="text-gray-300 w-4 h-4" />
      ))}
    </div>
  );
}

const Recipe = ({ recipe: data }: { recipe: RecipeData }): JSX.Element => {
  const [showDetails, setShowDetails] = useState(false);

  const toggleDetails = (): void => {
    setShowDetails(!showDetails);
  };

  return (
    <div className="bg-white shadow-md rounded-md overflow-hidden col-span-4 p-2 m-2">
      <div className="p-4">
        <h1 className="text-2xl font-semibold">{data.name}</h1>
        <p className="text-sm font-medium text-gray-500 line-clamp-2 h-10">
          {data.description}
        </p>
      </div>
      {showDetails && (
        // Center with 50% width and height and add backdrops
        <>
          <div className="fixed top-0 left-0 w-full h-full bg-black bg-opacity-50 z-10"></div>
          <div className="p-4 space-y-2 absolute top-1/3 left-1/3 transform -translate-x-1/3 -translate-y-1/3 w-3/4 h-3/4 bg-white shadow-md rounded-md overflow-auto z-20">
            {/* Close button */}
            <button
              className="absolute top-0 right-0 p-4"
              onClick={toggleDetails}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="h-6 w-6 text-gray-500 hover:text-gray-700"
                fill="none"
                viewBox="0 0 24 24"
                stroke="currentColor"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  strokeWidth="2"
                  d="M6 18L18 6M6 6l12 12"
                />
              </svg>
            </button>
            <h1 className="text-xl uppercase text-center underline">
              {data.name}
            </h1>
            <div className="grid grid-cols-12">
              <div className="col-span-8">
                <div className="my-2">
                  <h2 className="text-lg font-medium">Ingredients</h2>
                  {data.ingredients.split(",").map((ingredient, index) => (
                    <span
                      key={index}
                      className={`inline-block rounded-full px-2 py-1 m-1 text-sm font-semibold text-gray-700 mr-2 ${
                        colors[index % colors.length]
                      }`}
                    >
                      {ingredient}
                    </span>
                  ))}
                </div>

                <div className="my-2">
                  <h2 className="text-lg font-medium">Steps</h2>
                  {data.steps.split("\n").map((step, index) => (
                    <p key={index} className="text-gray-500">
                      {index + 1}. {step}
                    </p>
                  ))}
                </div>

                <div className="my-2">
                  <h2 className="text-lg font-medium">Tags</h2>
                  {data.tags.split(",").map((tag, index) => (
                    <span
                      key={index}
                      className={`inline-block rounded-full px-2 py-1 m-1 text-sm font-semibold text-gray-700 mr-2 ${
                        colors[index % colors.length]
                      }`}
                    >
                      {tag}
                    </span>
                  ))}
                </div>

                <div className="my-2">
                  <h2 className="text-lg font-medium">Rating</h2>
                  <Rating rating={data.rating} />
                </div>

                <div className="my-2">
                  <h2 className="text-lg font-medium">Minutes to make</h2>
                  <p className="text-gray-500">{data.minutes}</p>
                </div>
                <div className="my-2">
                  <h2 className="text-lg font-medium">Food Type</h2>
                  <p className="text-gray-500">{data.food_types}</p>
                </div>
              </div>
              <div className="col-span-4">
                <div className="my-2">
                  <h2 className="text-lg font-medium">Nutrition Information</h2>
                  <table className="divide-y divide-gray-200">
                    <thead className="bg-gray-50">
                      <tr>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Nutrition Information
                        </th>
                        <th
                          scope="col"
                          className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                        >
                          Amount per serving
                        </th>
                      </tr>
                    </thead>
                    <tbody className="bg-white divide-y divide-gray-200">
                      <tr>
                        <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                          Calories
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {data.calories}
                        </td>
                      </tr>
                      <tr>
                        <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                          Protein
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {data.protein}g
                        </td>
                      </tr>
                      <tr>
                        <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                          Carbohydrates
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {data.carbohydrates}g
                        </td>
                      </tr>
                      <tr>
                        <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                          Sugar
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {data.sugar}g
                        </td>
                      </tr>
                      <tr>
                        <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                          Total Fat
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {data.total_fat}g
                        </td>
                      </tr>
                      <tr>
                        <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                          Saturated Fat
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {data.saturated_fat}g
                        </td>
                      </tr>
                      <tr>
                        <td className="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900">
                          Sodium
                        </td>
                        <td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
                          {data.sodium}mg
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
      <div className="p-4 text-right">
        <button
          className="text-blue-500 font-medium focus:outline-none"
          onClick={toggleDetails}
        >
          {showDetails ? "Hide Details" : "Show Details"}
        </button>
      </div>
    </div>
  );
};

export const Recipes = ({
  recipes,
}: {
  recipes: RecipeData[];
}): JSX.Element => {
  const [recipeData, setRecipeData] = useState<RecipeData[]>(recipes);

  useEffect(() => {
    setRecipeData(recipes);
  }, [recipes]);

  return (
    <div className="grid grid-cols-12 bg-gray-100">
      {recipes.map((recipe, index) => (
        <Recipe recipe={recipe} key={recipe.recipe_id} />
      ))}
    </div>
  );
};
