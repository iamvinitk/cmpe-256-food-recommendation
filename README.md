# CMPE-256-Food-Recommendation


## Introduction
As more people use technology to find new recipes and plan their meals, we have begun to rely on instructions from various websites in the kitchen and hence, making food recommendation systems more and more popular. Based on the user’s dietary preferences, nutritional information, time taken and past interaction with recipes, these systems provide customized recommendations. Moreover, the complicated relationships between food, user preferences, and other factors make it challenging to design meal recommendation systems. 

Users would like to see other types of food recommendations rather than their usual preferences but similar to their dietary choices. A lot of people would like to try new recipes based on their previous preferences. These users cannot rely on existing recipe generations given the type of food they wish to eat or want the recipes and other related information for the same. Such models will also give incomplete recipe information. 

Our aim is to propose similar recipes given the type of recipe that the user wishes to eat. Our project combines two approaches: recommender systems and natural language processing. 

## Team Members
- [Meera Tresa Sebastian](https://gitlab.com/MeeraTresa/)
- [Sanika Vijaykumar Karwa](https://gitlab.com/sanika-karwa/)
- [Vinit Kanani](https://gitlab.com/iamvinitk/)

## Dataset
[Food.com Recipes and Interactions](https://www.kaggle.com/datasets/shuyangli94/food-com-recipes-and-user-interactions)

## Running the code
1. Clone the repository
2. Install the requirements using `pip install -r requirements.txt`
3. Backend: 
   - `cd backend`
   - `flask --app main run`
4. Frontend:
   - `cd ui-app`
   - `npm install`
   - `npm start`


