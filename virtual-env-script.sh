#!/bin/bash

# Create a new virtual environment called "env" in the current directory
python3 -m venv env

# Activate the virtual environment
source env/bin/activate

# Install any packages you need using pip
pip install -r requirements.txt

# Deactivate the virtual environment when you're done
deactivate
