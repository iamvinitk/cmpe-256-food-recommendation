import pandas as pd

from cdl import get_similar_recipes_cdl
from hybrid import get_similar_recipes_content
from nmf import get_nmf_recommendations


df = pd.read_csv('../dataset/preprocessed_data.csv')

# select random 3 recipes
recipe_ids = [6]

# get recommendations from nmf
for recipe_id in recipe_ids:
    nmf_recommendations = get_nmf_recommendations(df, recipe_id=recipe_id)
    nmf_recipe_ids = [{'recipe_id': recipe['recipe_id'], 'similarity': recipe['similarity']} for recipe in nmf_recommendations]
    print("NMF Recommendations ==>> ", nmf_recipe_ids)

# get recommendations from cdl
for recipe_id in recipe_ids:
    cdl_recommendations = get_similar_recipes_cdl(df, recipe_id=recipe_id)
    cdl_recipe_ids = [{'recipe_id': recipe['recipe_id'], 'similarity': recipe['similarity']} for recipe in cdl_recommendations]
    print("CDL Recommendations ==>> ", cdl_recipe_ids)