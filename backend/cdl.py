import faiss
import numpy as np
import pandas as pd

recipe_content_embedding = np.load('../models/X_with_recipe_embeddings.npy')
print("Recipe Embedding ==>> ", recipe_content_embedding.shape)

content_embedding = recipe_content_embedding[:, :-2]
recipe_embedding = recipe_content_embedding[:, -2]
user_embedding = recipe_content_embedding[:, -1]

# convert to int for recipe and user embeddings
recipe_embedding = recipe_embedding.astype(int)
user_embedding = user_embedding.astype(int)

index = faiss.IndexFlatL2(content_embedding.shape[1])
index.add(content_embedding)


def get_similar_recipes_cdl(df, recipe_id):
    recipe_id = int(recipe_id)
    # get the recipe index from the recipe id
    recipe_index = np.where(recipe_embedding == recipe_id)[0][0]
    recipe_vector = content_embedding[recipe_index]

    # search for the nearest neighbors
    k = 400
    D, I = index.search(np.array([recipe_vector]), k)
    print("I ==> ", I.shape)
    # get recipe ids from X_with_recipe_embeddings using I
    recipe_ids = recipe_embedding[I[0]]
    # remove the target recipe id from the list
    recipe_ids = recipe_ids[recipe_ids != recipe_id]

    recipe_ids = recipe_ids.astype(int)

    top_n = df[df['recipe_id'].isin(recipe_ids)].drop_duplicates(subset=['recipe_id'])
    # add the distance as a column by matching the index of recipe_ids in I
    # top_n['similarity'] = D[0][np.where(recipe_ids == top_n['recipe_id'].values[0])[0][0]]

    top_n = top_n.where(pd.notnull(top_n), None)
    top_n = top_n.head(10)
    return top_n.to_dict(orient='records')

def get_similar_recipe_ids_cdl(recipe_id):
    recipe_id = int(recipe_id)
    # get the recipe index from the recipe id
    recipe_index = np.where(recipe_embedding == recipe_id)[0][0]
    recipe_vector = content_embedding[recipe_index]

    # search for the nearest neighbors
    k = 400
    D, I = index.search(np.array([recipe_vector]), k)
    print("I ==> ", I.shape)
    # get recipe ids from X_with_recipe_embeddings using I
    recipe_ids = recipe_embedding[I[0]]
    # remove the target recipe id from the list
    recipe_ids = recipe_ids[recipe_ids != recipe_id]

    recipe_ids = recipe_ids.astype(int)

    return recipe_ids.tolist()
