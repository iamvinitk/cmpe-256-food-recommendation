import pickle

import faiss
import numpy as np
import pandas as pd
from scipy.sparse import load_npz

from cdl import get_similar_recipe_ids_cdl

X = load_npz('../models/sparse_data.npz')

X = X.toarray()
content_embedding = X[:, :-2]
recipe_embedding = X[:, -2]
user_embedding = X[:, -1]

# convert to int for recipe and user embeddings
recipe_embedding = recipe_embedding.astype(int)
user_embedding = user_embedding.astype(int)

print("Content Embedding ==>> ", X.shape)

index = faiss.IndexFlatL2(content_embedding.shape[1])
index.add(content_embedding)

print(index.ntotal)

filename = '../models/svd_model.sav'
algo = pickle.load(open(filename, 'rb'))


def get_similar_recipes_content(recipe_id):
    recipe_id = int(recipe_id)
    recipe_index = np.where(recipe_embedding == recipe_id)[0][0]
    recipe_vector = content_embedding[recipe_index]
    # search for the nearest neighbors
    k = 1000
    D, I = index.search(np.array([recipe_vector]), k)
    recipe_ids = recipe_embedding[I[0]]
    unique_recipe_ids = np.unique(recipe_ids)
    unique_recipe_ids = unique_recipe_ids[unique_recipe_ids != recipe_id]
    return unique_recipe_ids.tolist()


def get_similar_recipes_svd(df, recipe_id):
    recipe_ratings = df[df.recipe_id == recipe_id]
    recipe_ratings['rating'] = recipe_ratings['rating'].astype(float)
    recipe_mean_rating = recipe_ratings['rating'].mean()

    test_set = [[user_id, recipe_id, recipe_mean_rating] for user_id in df.user_id.unique()]
    predictions = algo.test(test_set)

    # filter predictions for the target recipe
    target_predictions = [pred for pred in predictions if pred.iid == recipe_id]

    # sort target predictions by estimated rating
    target_predictions.sort(key=lambda x: x.est, reverse=True)
    print("Target Prediction", target_predictions[:10])
    top_n_recipes = [pred.uid for pred in target_predictions]
    return top_n_recipes


def get_similar_recipes_hybrid(df, recipe_id, n=10):
    content = get_similar_recipes_content(recipe_id)
    svd = get_similar_recipe_ids_cdl(recipe_id)
    print("Collab: ", len(content))
    print("SVD: ", len(svd))
    # find the intersection of the two lists
    similar_recipes = list(set(content) & set(svd))
    # similar_recipes = list(set(content))
    similar_recipes = [int(x) for x in similar_recipes]
    print("Intersection: ", len(similar_recipes))
    similar_recipes = similar_recipes[:n]
    top_n_recipe_data = df[df.recipe_id.isin(similar_recipes)].drop_duplicates(subset=['recipe_id'])

    # replace NaN with None
    top_n_recipe_data = top_n_recipe_data.where(pd.notnull(top_n_recipe_data), None)
    top_n_recipe_data = top_n_recipe_data.head(n)
    top_n_recipe_data = top_n_recipe_data.to_dict(orient='records')
    return top_n_recipe_data
